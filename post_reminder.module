<?php
/**
 * @file
 * This module allows users to establish reminders which will be sent to them if they do not post for a set length  of time
 */


/**
 * implementation of hook_help()
 */
function post_reminder_help($path, $arg) {
  if ($path == 'admin/help#post_reminder') {
    $text = 'This module allows users to establish reminder which will be sent to them if they do not post for a set length  of time';
    return t($text);
  }
}

/**
 * implementation of hook_menu()
 */
function post_reminder_menu() {
  $items = array();
  $items['user/%user_category/edit/post_reminder'] = array(
    'title' => 'Post Reminders',
    'file' => 'post_reminder.pages.inc',
    'page callback' => 'post_reminder_user_settings',
    'page arguments' => array(1),
    'access arguments' => array('set post reminders'),
    'load arguments' => array('%map', '%index'),
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/settings/post_reminder'] = array(
    'title' => 'Post Reminder Settings',
    'description' => 'Change settings for the Post Reminder module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('post_reminder_admin_settings'),
    'access arguments' => array('administer reminders'),
    'file' => 'post_reminder.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implementation of hook_user().
 */
function post_reminder_user($op, &$edit, &$account, $category = NULL) {
  switch ($op) {
    case 'categories':
      return array(
        array(
          'name' => 'post_reminder',
          'title' => 'Post Reminders',
          'weight' => 3,
        ),
      );
  }
}

/**
 * implementation of hook_node_info()
 */
function post_reminder_node_info() {
  $info = array(
    'post_reminder' => array(
      'name' => t('Post Reminder'),
      'description' => t('A reminder which will be sent after the given time has elapsed since your last post'),
      'has_title' => TRUE,
      'title_label' => t('Reminder Name'),
      'has_body' => FALSE,
      'module' => 'post_reminder',
    ),
  );

  return $info;
}

/**
 * implementation of hook_form()
 */
function post_reminder_form(&$node) {

  if (!allowed_reminder_types()) {
    drupal_set_message(t("No content types permit reminders to be created for them. Please contact your site administrator."), 'error');
  } 

  $type = node_get_types('type', $node);
  $form = array();
  $form['#validate'][] = 'post_reminder_form_validate';


  if ($type->has_title) {
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => check_plain($type->title_label),
      '#required' => TRUE,
      '#default_value' => $node->title,
      '#weight' => -5,
    );
  }
  if ($type->has_body) {
    $form['body_field'] = node_body_field(
      $node,
      $type->body_label,
      $type->min_word_count
    );
  }

  //content type
  $form['content_type'] = array(
    '#title' => t('Content Type'),
    '#description' => t('The content type for which the reminder is set'),
    '#type' => 'select',
    '#options' => allowed_reminder_types(),
    '#default_value' => $node->content_type,
    '#required' => TRUE,
  );

  //period
  $form['amount_of_time'] = array(
    '#title' => t('Amount of time'),
    '#description' => t('The amount of time which will trigger the reminder.  E.G., 3, as in \'3 days\''),
    '#type' => 'textfield',
    '#maxlength' => 2,
    '#size' => 2,
    '#default_value' => $node->amount_of_time,
    '#required' => TRUE,
  );

  //scale
  $form['unit_of_time'] = array(
    '#title' => t('Unit of time'),
    '#type' => 'select',
    '#options' => array(
      'hours' => t('Hours'), 
      'days' => t('Days'), 
      'weeks' => t('Weeks'), 
      'months' => t('Months'),
    ),
    '#default_value' => $node->unit_of_time,
    '#required' => TRUE,
  );

  //active
  $form['active'] = array(
    '#title' => t('Active'),
    '#description' => t('Uncheck to hold the reminder without deleting it'),
    '#type' => 'checkbox',
    '#default_value' => '1',
  );

  return $form;
}

/**
 *  validate post reminder form
 */
function post_reminder_form_validate($form, &$form_state) {
  $amount_of_time = $form_state['values']['amount_of_time'];
  
  //ensure amount_of_time is whole number
  if (!is_numeric($amount_of_time) || (int) $amount_of_time != $amount_of_time) {
    form_set_error('amount_of_time', t('Amount of time must be an integer'));
  }
}


/**
 * implementation of hook_access()
 */
function post_reminder_access($op, $node, $account) {
  switch ($op) {
    case 'create':
      return user_access('set reminder', $account);
    case 'update':
    case 'delete':
    case 'view':
      //access only permitted for reminder owners
      if ($account->uid == $node->uid) {
        return TRUE;
      }
      else {
        return FALSE;
      }
  }

}

/**
 * implementation of hook_insert()
 */
function post_reminder_insert($node) {
  db_query('insert into {post_reminder} (vid, nid, content_type, amount_of_time, unit_of_time, active)' .
    "values (%d, %d, '%s', %d, '%s', %d)", 
    $node->vid,
    $node->nid,
    $node->content_type,
    $node->amount_of_time,
    $node->unit_of_time,
    $node->active);
}

/**
 * implementation of hook_update()
 */
function post_reminder_update($node) {
  if ($node->revision) {
    post_reminder_insert($node);
  }
  else {
    db_query("update {post_reminder} set " .
      "content_type = '%s', amount_of_time = %d, unit_of_time = '%s', active = %d",
      $node->content_type, $node->amount_of_time, $node->unit_of_time, $node->active);
  }
}

/**
 * implementation of hook_delete()
 */
function post_reminder_delete($node) {
  db_query("delete from {post_reminder} where nid = %d", $node->nid);
}

/**
 * implementation of hook_nodeapi()
 *
 * just handles revision deletion
 */
function post_reminder_nodeapi(&$node, $op, $a3, $a4) {
  if ($op == 'delete revision') {
    db_query("delete from {post_reminder} where vid = %d", $node->vid);
  }
}

/**
 * implementation of hook_load()
 */
function post_reminder_load($node) {
  $result = db_query("select content_type, amount_of_time, unit_of_time, active, last_sent from " .
      "{post_reminder} where vid = %d", $node->vid);
  return db_fetch_object($result);
}

/**
 * implementation of hook_view()
 */
function post_reminder_view($node, $teaser = FALSE, $page = FALSE) {
  $node = node_prepare($node, $teaser);
  
  $content_type = node_get_types('name', check_plain($node->content_type));  //display human_readable name, not internal name
  $amount_of_time = check_plain($node->amount_of_time);
  $unit_of_time = check_plain($node->unit_of_time);
  $active = check_plain($node->active);

  $node->content['post_reminder'] = array(
    '#value' => theme('post_reminder_info', $content_type, $amount_of_time, $unit_of_time, $active),
    '#weight' => 1,
  );
  return $node;
}

/**
 * implementation of hook_theme()
 */
function post_reminder_theme() {
  $themes = array();
  $themes['post_reminder_info'] = array(
    'template' => 'post_reminder_info',
    'arguments' => array(
       'content_type' => NULL, 
       'amount_of_time' => NULL,
       'unit_of_time' => NULL,
       'active' => NULL,
     ),
  );
  return $themes;
}

/**
 * implementation of hook_cron()
 */
function post_reminder_cron() {
  process_reminders();
}

/**
 * implementation of hook_perm()
 */
function post_reminder_perm() {
  return array(
    'set reminder',  //set reminder for self
  );
}

/**
 * implementation of hook_mail()
 */
function post_reminder_mail($key, &$message, $params) {
  $reminder = $params['reminder'];
  if ($key == 'reminder' ) {
    $message['subject'] = token_replace(reminder_subject(), 'node', $reminder);; 
    $message['body'] = token_replace(reminder_body(), 'node', $reminder);
  }
}

/**
 * implementation of hook_token_values()
 */
function post_reminder_token_values($type, $object = NULL, $options = array()) {
  if ($type == 'post_reminder' || $type == 'node') { 
    $reminder = $object;
    $type_names = node_get_types('names');
    $tokens = array();
    $tokens['reminder_interval'] = t("@amount @unit", array('@amount' => $reminder->amount_of_time, '@unit' => $reminder->unit_of_time));
    $tokens['reminder_content_type'] = $type_names[$reminder->content_type];
    $tokens['reminder_url'] = url("node/{$reminder->nid}", array('absolute' => TRUE));  
    return $tokens;
  }
}

/**
 * implementation of hook_token_list()
 */
function post_reminder_token_list($type = 'all') {
  if ($type == 'post_reminder' || $type == 'node' || $type == 'all') {
    $tokens = array();
    $tokens['post_reminder']['reminder_interval'] = t('The amount and unit of time after which a reminder will be sent (e.g., 3 days, 6 weeks)');
    $tokens['post_reminder']['reminder_content_type'] = t('The content type to which the reminder is tied (e.g., Blog, Story)');
    $tokens['post_reminder']['reminder_url'] = t('An absolute url for the reminder, suitable for sending in emails so users know which reminder triggered the message');
    return $tokens;

  }
}
// MODULE FUNCTIONS

/**
 * get current message subject text, either custom or defualt if unset
 * @return
 *  string
 */
function reminder_subject() {
  $default = "[site-name]: Reminder to Post";
  return variable_get('post_reminder_message_subject', $default);
 }

/**
 * get current message body text, either custom or defualt if unset
 * @return
 *  string
 */
function reminder_body() {
  $default = "You configured this reminder to be sent to you [reminder_interval] after you last posted a [reminder_content_type]\n\n";
  $default .= "----------------\n"; 
  $default .= "This message is sent based on the reminder at [reminder_url]"; 

  return variable_get('post_reminder_message_body', $default);
 }

/**
 * return array of content types - not including post_reminder type
 * @return 
 *   array
 */
function remindable_types() {
  $raw_types = node_get_types('names');
  if (array_key_exists('post_reminder', $raw_types)) {
    unset($raw_types['post_reminder']);
  }
  return $raw_types;
}

/**
 * return array of content types selected as remindable by admin
 * @return 
 *   array
 */
function allowed_reminder_types() {
  $allowed = array();
  $types = variable_get('post_reminder_node_types', FALSE);
  $raw_types = node_get_types('names');  //so we have a source for human-readable type names
  if ($types) {
    foreach ($types as $type => $value) {
      if ($value) {
        $allowed[$type] = $raw_types[$type];
      }
    }
  }
  return $allowed;
}

/**
 * process reminders
 *
 * controller function that calls list_reminders() and evaluate_reminder()
 */
function process_reminders() {
  // get list of reminders
  $reminders_list = list_reminders();

  if (!$reminders_list) {
    return;
  }

  foreach ($reminders_list as $reminder) {
    //evaluate reminder
    if (evaluate_reminder($reminder)) {
      send_reminder($reminder);
    }
  }
}

/**
 * get list of active reminders from the database
 *
 * @return 
 *   array of reminder objects
 *
 * @todo
 *   restrict list to reminder owned by active users
 */
function list_reminders() {
  $reminder_sql = "select n.uid, n.nid, n.vid, pr.content_type, pr.amount_of_time, pr.unit_of_time, pr.active, pr.last_sent
    from {node} as n left join {post_reminder} as pr 
    on n.nid = pr.nid and n.vid = pr.vid
    where n.type = 'post_reminder' and pr.active = 1";

  $reminders_result = db_query($reminder_sql);
  $reminders = array();
  while ($reminder = db_fetch_object($reminders_result)) {  
    $reminders[] = $reminder;
  }
  return $reminders;
}

/**
 *  evaluate reminder
 *  
 *  @param
 *    reminder - reminder object
 *
 *  @return 
 *    boolean - TRUE if criteria met for sending reminder
 *
 *  @todo
 *    make interval configurable
 */
function evaluate_reminder($reminder) {
  $sql = "select max(created) as created from {node} where status = 1 and uid = %d and type = '%s'";
  $last_post = db_fetch_object(db_query($sql, $reminder->uid, $reminder->content_type));
  if (
     (!$last_post->created 
     || time() > strtotime(date('Y-m-d', $last_post->created) . "+ {$reminder->amount_of_time} {$reminder->unit_of_time}"))
     && (($reminder->last_sent + (60*60*24)) < mktime()) //only send one reminder every 24 hours
     ) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * send reminder
 *
 * @param
 *   reminder - a reminder object which needs to be sent
 *
 * @return
 *   array - as returned by drupal_mail()
 *
 * @todo
 *   messaging integration
 */
function send_reminder($reminder) {
  $user_account = user_load($reminder->uid);
  $params = array(
    'reminder' => $reminder,
    'user_account' => $user_account,
  );
  $result = drupal_mail('post_reminder', 'reminder', $user_account->mail, user_preferred_language($user_account), $params);
  if ($result) {
    set_last_sent($reminder);
  }
  return $result;
}

/**
 * update the last_sent timestamp
 *
 * @param
 *   reminder object
 */
function set_last_sent($reminder) {
  $sql = "update {post_reminder} set last_sent = %d where vid = %d";
  db_query($sql, mktime(), $reminder->vid);
}

/**
 * list remidners for a given user
 *
 * @param
 *   user id
 * @return
 *   array of reminder objects
 */
function list_user_reminders($uid) {
  $sql = "select pr.nid, pr.vid, n.title, content_type, amount_of_time, unit_of_time, active " .
         "from post_reminder pr join node n " .
         "on pr.vid = n.vid " .
         "where uid = %d";
  $result_set = db_query($sql, $uid);
  $reminders = array();
  
  while ($reminder = db_fetch_object($result_set)) {
    $reminders[] = $reminder;
  }  
  return $reminders;
}
