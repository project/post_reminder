/* $Id */
Post Reminder module

Description:
The Post Reminder module allows users with permissions to set reminders which will
be sent to them via email after the set length of time has passed since their last
post of a given content type.

Installation:
Same as any other Drupal module.  Unpack the module in your modules directory, enable it 
through the site's admin/build/modules interface and viola.

Reminder data is stored in a {post_reminder} table, so if removing the module be sure to run the 
uninstall routine to drop that table.

Installation of the Token module is required.

Administration:
Two areas need attention after installationi: 

* First, an administrator must set the content types
for which reminders are allowed to be set.  This can be accomplished via the form in 
/admin/settings/post_reminder.  If no content types permit reminders, users
will see an error message to that effect displayed on the reminder creation
form.

* Second, any roles which are allowed to set reminders must be given 
the set "reminder permission"

* Optionally, at the /admin/settings/post_reminder page, an admin may change the text of
the message body and message subject which will be sent when a Post Reminder
message is triggered.  Both these fields support Token replacements using
fields specific to the Post Reminder type or Nodes in general.

Use:
Reminders are established as content types, so creation is the same as any other content type.  

Five fields are contained, all of which are required.  The Reminder Name field simply serves as an identifier
for the reminder.  The Content Type field associates the reminder with the content type to which it is 
tied.  For instance, if you want a reminder when you haven't posted a Blog in some length of time, select
"Blog" for the Content Type.  The Amount of Time and Unit of Time fields together establish the interval since 
the last post at which a reminder will be sent.  (This is not necessarily precisely when the reminder will be sent though.
See the below for details.)  Finally, the Active field allows reminders to be turned on an off without having to delete them.
Inactive reminders are not sent.

At each cron run, active reminders will be evaluated to see if their interval has elapsed.  It is up to site admins to set
an appropriate frequency for cron runs.  Shorter frequencies will result in more precise reminders but greater overhead.  Longer
frequencies will save overhead but reminders will be less precisely sent.  Only new posts count toward this interval.  Editing
a post will not defer the reminder.

A user can view all reminders they have, regardless of Active/Inactive on the
Post Reminder tab of their user account page when in edit mode.
