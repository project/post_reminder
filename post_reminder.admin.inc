<?php
/**
 * @file
 * Administration page callbacks for the Post Remminder module
 */

/**
 * Form builder. Configure Post Reminders.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function post_reminder_admin_settings() {
  $form['post_reminder_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Users may set reminders for these content types'),
    '#options' => remindable_types(),
    '#default_value' => variable_get('post_reminder_node_types', FALSE),
  );

  $form['post_reminder_message_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject of Post Reminder messages'),
    '#required' => TRUE,
    '#default_value' => reminder_subject(),
    '#description' => t('Token replacements for node types are supported'),
  );

  $form['post_reminder_message_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of Post Reminder messages'),
    '#required' => TRUE,
    '#default_value' => reminder_body(), 
    '#description' => t('Token replacements for node types are supported'),
  );

  $form['view']['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['view']['token_help']['help'] = array(
    '#value' => theme('token_help', 'node'),
  );

  return system_settings_form($form);
}
