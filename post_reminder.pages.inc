<?php
/**
 * @file
 * Pages for the Post Reminder module
 */
/**
 *  Render page form for setting reminders and list of current reminders
 */
function post_reminder_user_settings($user_id) {
  $output = '<div>' . l('Create a Post Reminder', 'node/add/post-reminder') . '</div>';
  $output .= post_reminder_user_reminder_table($user_id);

  return $output;
}

/**
 *  Render form for setting reminders
 */
function post_reminder_user_reminder_table($user_id) {
  $output = '';
 
  $type_names = node_get_types('names');
 
  $row_headers = array('Title', 'Content Type', 'Interval', 'Active?');
  $rows = array();
  
  $reminders = list_user_reminders($user_id);
  if (!$reminders) {
    $output .= t("No reminders found");
  } 
  else {
    foreach ($reminders as $reminder) {
      $data_array = array(
        l($reminder->title, "node/{$reminder->nid}"),
        $type_names[$reminder->content_type],
        $reminder->amount_of_time . " " . $reminder->unit_of_time,
        ($reminder->active ? t("Active") : t("Inactive")),
      );
    $rows[] = $data_array; 
    }
    $output .= theme('table', $row_headers, $rows);
  }
  
  return $output;
}
